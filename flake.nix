{
  description = "AsciiDoctor-pdf flake to batch-compile with custom theming";

  # Basic nixpkgs and flake-utils
  inputs.nixpkgs.url     = github:NixOS/nixpkgs/nixos-21.05;
  inputs.flake-utils.url = github:numtide/flake-utils;

  # Path to docs (source code) and themes
  # TODO: don't hardcode user home, use flake directory instead
  inputs.documents.url   = "path:/home/alejandro/fde-documentos/docs";
  inputs.documents.flake = false;
  inputs.themes.url   = "path:/home/alejandro/fde-documentos/themes";
  inputs.themes.flake = false;

  outputs = { self, nixpkgs, flake-utils, documents, themes }:
    flake-utils.lib.eachDefaultSystem (system:
    let
      # Packages we'll need from nixpkgs
      pkgs = nixpkgs.legacyPackages.${system};
      d-din = pkgs.fetchzip { # Font
        name = "D-DIN";
        url = "https://fontlibrary.org/assets/downloads/d-din/39fc8c3de156292478f87c2ff0b96df2/d-din.zip";
        sha256 = "sha256-5F+/mRvuqv6XNOTkPKSDu+7nnh3xGkJkpnwp9IrPsbY=";
        # This is to signal the builder that we have list of files
        stripRoot = false;
      };
    in rec {
      packages.document = pkgs.stdenvNoCC.mkDerivation rec {
        name = "asciidoctor-pdf-compiled-document";
        src = self;
        buildInputs = [ pkgs.findutils pkgs.coreutils pkgs.asciidoctor pkgs.pandoc pkgs.texlive.combined.scheme-full pkgs.gnused ];
        phases = [ "unpackPhase" "buildPhase" ];
        buildPhase =''
          # Add buildInputs to $PATH
          export PATH="${pkgs.lib.makeBinPath buildInputs }";

          # Search and find source files
          export SRC_FILES="$(find ${documents} -type f -name "*.adoc")";
          echo "Generando PDF";
          # Convert .adoc -> .pdf, asciidoctor-pdf can mirror directory structure input-output
          ${pkgs.asciidoctor}/bin/asciidoctor-pdf -a pdf-themesdir=${themes} -a pdf-fontsdir=${d-din},GEM_FONTS_DIR -R ${documents} -D $out $SRC_FILES;
          echo "Generando DOCBOOK";
          # Convert .adoc -> docbook (.xml) to later use with pandoc
          ${pkgs.asciidoctor}/bin/asciidoctor-pdf --backend docbook -a pdf-themesdir=${themes} -a pdf-fontsdir=${d-din},GEM_FONTS_DIR -R ${documents} -D $out $SRC_FILES;
          echo "Generando DOCX";
          export DOC_FILES="$(find $out -type f -name "*.xml")"; # Search and find docbook intermediate format
          for f in $DOC_FILES; do
            export wordfname="$(sed 's|xml|docx|g' <<< $f)"; # Rename extension (cant use bash substitution (why nix?)
            echo "$f -> $wordfname";
            # Convert using pandoc (respects directory structure)
            ${pkgs.pandoc}/bin/pandoc --from docbook --to docx -i $f -o $wordfname && rm $f;
          done;
          '';
      };
      defaultPackage = packages.document;
    });
}
